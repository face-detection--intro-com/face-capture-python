import cv2
import sys
import photomessage_pb2 as photomessage
import time
import base64
import sys
import socket
from google.protobuf.internal import encoder

IP = '127.0.0.1'
PORT = 6000

def send(message,sock):
    delimiter = encoder._VarintBytes(len(message))
    message = delimiter + message
    msg_len = len(message)
    
    total_sent = 0
    while total_sent < msg_len:
        sent = sock.send(message[total_sent:])
        if sent == 0:
            raise RuntimeError('socket connection broken')
        total_sent = total_sent + sent

#Generate message from image
def toProtoBuf(image):
    message = photomessage.Cameramessage()
    message.id = 1
    message.utc_time = int(round(time.time() * 1000))
    message.base64_image = image

    return message.SerializeToString()

#Main routine
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((IP, PORT))
faceCascade = cv2.CascadeClassifier("../resource/haarcascade_frontalface_default.xml")
video_capture = cv2.VideoCapture(0)
imageCounter = 0
while True:
    # Capture frame-by-frame
    ret, frame = video_capture.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    imageCounter += 1
    personCounter = 0
    # crop all faces and send messages
    for (x, y, w, h) in faces:
        r = max(w, h) / 2
        centerx = x + w / 2
        centery = y + h / 2
        nx = int(centerx - r) -25
        ny = int(centery - r) -25
        nr = int(r * 2) + 50
        faceimg = frame[ny:ny+nr, nx:nx+nr]
        personCounter += 1
        #cv2.imwrite("../output/image%d-person%d.jpg" % (imageCounter,personCounter) , faceimg)
        data = cv2.imencode(".png", faceimg)[1].tostring()
        send(toProtoBuf(base64.b64encode(data)),sock)

    #Wait half a second before taking a new frame
    key = cv2.waitKey(1)
    # Display the resulting frame
#    cv2.imshow('Video', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()
